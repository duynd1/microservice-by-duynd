package com.example.productservice.entity;

import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;

@Component
@ToString
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "duynd_product")
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 5,nullable = false)
    private int id;
    @Column(length = 50,nullable = false)
    private String name;
    @Column(length = 11,nullable = false)
    private double price;
    @Column(length = 5)
    private int quantity;
    private int category_Id;
    private int brand_Id;
}
