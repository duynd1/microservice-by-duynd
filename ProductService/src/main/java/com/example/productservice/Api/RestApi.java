package com.example.productservice.Api;

import com.example.productservice.dto.CategoryDto;
import com.example.productservice.dto.ProductDto;
import com.example.productservice.feignClient.BrandClient;
import com.example.productservice.feignClient.CategoryClient;
import com.example.productservice.repository.ProductRepository;
import com.example.productservice.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/")
public class RestApi {

    @Autowired
    ProductService productService;
    @Autowired
    private CategoryClient categoryClient;
    @Autowired
    BrandClient brandClient;
    @Autowired
    ProductRepository repo;
    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/all")
    public ResponseEntity<?> getAll(){
        List<ProductDto> productDtos = productService.getAll();
        for (ProductDto x :productDtos){
            x.setBrand(brandClient.getById(x.getBrandId()));
            x.setCategory(categoryClient.getById(x.getCategoryId()));
        }
        return ResponseEntity.ok(productDtos);
    }

    @GetMapping("/allcategory")
    public ResponseEntity<?> getAllCatogry(){
        System.out.println("call");
        return ResponseEntity.ok(categoryClient.getAll());
    }


    @GetMapping("/allbrand")
    public ResponseEntity<?> getAllBrand(){
        return ResponseEntity.ok(brandClient.getAll());
    }

    @GetMapping("/getby{id}")
    public ResponseEntity<?> getById(@RequestParam int id){
        ProductDto dto = productService.getById(id);
        dto.setBrand(brandClient.getById(dto.getBrandId()));
        return ResponseEntity.ok(dto);
    }

    @GetMapping("/getbybrand{brandId}")
    public ResponseEntity<?> getByBrandId(@RequestParam("brandId") int brandId){
        System.out.println("called");
        return ResponseEntity.ok(productService.getByBrand(brandId));
    }

    @PostMapping("/add")
    public ResponseEntity<?> create(@RequestBody ProductDto productDto){
        System.out.println(productDto.toString());
        return ResponseEntity.ok(productService.create(productDto));
    }

    public ResponseEntity<?> fallback(int id, Throwable hystrixCommand) {
        System.out.println("caller to here");
        return ResponseEntity.ok(productService.getById(id));
    }

    @GetMapping("/allmonolithic")
    public ResponseEntity<?> getAllbymonolithic(){
        List<ProductDto> lst = new ArrayList<>();
        ResponseEntity<?> responseEntity = restTemplate.getForEntity("all",Object.class);
        lst= (List<ProductDto>) responseEntity.getBody();
        return ResponseEntity.ok(lst);
    }
}
