package com.example.productservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BrandDto {
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
}
