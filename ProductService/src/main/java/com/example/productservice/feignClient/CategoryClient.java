package com.example.productservice.feignClient;


import com.example.productservice.dto.CategoryDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
@Service
// định nghĩa cho client này gọi đến category-service
@FeignClient(name = "category-service")
public interface CategoryClient {
    @GetMapping(value = "/all")
    List<CategoryDto> getAll();
    @GetMapping(value = "/getby{id}")
    CategoryDto getById(@RequestParam("id") int id);
}
