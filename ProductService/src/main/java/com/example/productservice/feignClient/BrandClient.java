package com.example.productservice.feignClient;

import com.example.productservice.dto.BrandDto;
import com.example.productservice.dto.CategoryDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
@FeignClient(name = "brand-service")
public interface BrandClient {
    @SuppressWarnings("unchecked")
    @GetMapping(value = "/all")
    List<BrandDto> getAll();
    @GetMapping(value = "/getby{id}")
    BrandDto getById(@RequestParam("id") int id);
}
