package com.example.productservice.service.serviceIpm;

import com.example.productservice.dto.ProductDto;
import com.example.productservice.entity.ProductEntity;
import com.example.productservice.feignClient.BrandClient;
import com.example.productservice.feignClient.CategoryClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Maper {


    public ProductDto mapToDto(ProductEntity entity){
        return ProductDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .price(entity.getPrice())
                .quantity(entity.getQuantity())
                .brandId(entity.getBrand_Id())
                .categoryId(entity.getCategory_Id())
                .build();
    }

    public ProductEntity mapToEntity(ProductDto dto){
        return ProductEntity.builder()
                .id(dto.getId())
                .name(dto.getName())
                .price(dto.getPrice())
                .quantity(dto.getQuantity())
                .brand_Id(dto.getBrandId())
                .category_Id(dto.getCategoryId())
                .build();
    }
}
