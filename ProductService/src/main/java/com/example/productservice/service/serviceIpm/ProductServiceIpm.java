package com.example.productservice.service.serviceIpm;

import com.example.productservice.dto.ProductDto;
import com.example.productservice.feignClient.BrandClient;
import com.example.productservice.feignClient.CategoryClient;
import com.example.productservice.repository.ProductRepository;
import com.example.productservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceIpm implements ProductService {
    @Autowired
    ProductRepository repo;
    @Autowired
    CategoryClient cateClient;
    @Autowired
    BrandClient brandClient;
    @Autowired
    Maper maper;

    @Override
    public boolean create(ProductDto productDto) {
        try{
            repo.save(maper.mapToEntity(productDto));
            return true;
        }catch (Exception x){
            x.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean update(ProductDto productDto) {
        try{
            repo.save(maper.mapToEntity(productDto));
            return true;
        }catch (Exception x){
            x.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(int id) {
        try{
            repo.deleteById(id);
            return true;
        }catch (Exception x){
            x.printStackTrace();
            return false;
        }
    }

    @Override
    public ProductDto getById(int id) {
        return maper.mapToDto(repo.findById(id).get());
    }

    @Override
    public List<ProductDto> getAll() {
        return repo.findAll().stream().map(p->maper.mapToDto(p)).collect(Collectors.toList());
    }

    @Override
    public List<ProductDto> getByCategory(int categoryId) {
        return repo.getByCategoryId(categoryId).stream().map(p->maper.mapToDto(p)).collect(Collectors.toList());
    }

    @Override
    public List<ProductDto> getByBrand(int brandId) {
        return repo.getByBrandId(brandId).stream().map(p->maper.mapToDto(p)).collect(Collectors.toList());
    }
}
