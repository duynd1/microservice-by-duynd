package com.example.productservice.service;

import com.example.productservice.dto.ProductDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {
    boolean create(ProductDto productDto);
    boolean update(ProductDto productDto);
    boolean delete(int id);
    ProductDto getById(int id);
    List<ProductDto> getAll();
    List<ProductDto> getByCategory(int categoryId);
    List<ProductDto> getByBrand(int brandId);
}
