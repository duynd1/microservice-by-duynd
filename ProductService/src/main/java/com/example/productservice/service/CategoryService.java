package com.example.productservice.service;

import com.example.productservice.dto.CategoryDto;

import java.util.List;

public interface CategoryService {
    CategoryDto getById(int id);
    List<CategoryDto> getAll();

}
