package com.example.productservice.service.serviceIpm;

import com.example.productservice.dto.CategoryDto;
import com.example.productservice.feignClient.CategoryClient;
import com.example.productservice.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CategoryServiceIpm implements CategoryService {
    @Autowired
    CategoryClient categoryClient;

    @Override
    public CategoryDto getById(int id) {
        return categoryClient.getById(id);
    }

    @Override
    public List<CategoryDto> getAll() {
        return categoryClient.getAll();
    }
}
