package com.example.productservice.repository;

import com.example.productservice.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity,Integer> {
    @Query("select p from ProductEntity p where p.category_Id = :categoryId")
    List<ProductEntity> getByCategoryId(@Param("categoryId") int categoryId);
    @Query("select p from ProductEntity p where p.brand_Id = :brandId")
    List<ProductEntity> getByBrandId(@Param("brandId") int brandId);
}
