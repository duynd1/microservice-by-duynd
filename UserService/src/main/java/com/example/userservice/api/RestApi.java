package com.example.userservice.api;

import com.example.userservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class RestApi {
    @Autowired
    UserRepository repo;

    @GetMapping("/getby{username}")
    public ResponseEntity<?> getByUsername(@RequestParam("username") String username){
        return ResponseEntity.ok(repo.findById(username).get());
    }
}
