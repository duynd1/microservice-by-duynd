package com.example.categoryservice.dto;

public class ProductDto {
    private int id;
    private String name;
    private double price;
    private int quantity;
}
