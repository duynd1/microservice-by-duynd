package com.example.categoryservice.dto;

import lombok.*;
import org.springframework.stereotype.Component;

@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@ToString
public class CategoryDto {
    private int id;
    private String name;
}
