package com.example.categoryservice.repository;

import com.example.categoryservice.Api.CategoryApi;
import com.example.categoryservice.entity.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<CategoryEntity,Integer> {
    @Procedure
    void duyND_GetProductByCategoryId(String name, String tpm);
}
