package com.example.categoryservice.Api;

import com.example.categoryservice.dto.CategoryDto;
import com.example.categoryservice.entity.CategoryEntity;
import com.example.categoryservice.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/")
public class CategoryApi {
    @Autowired
    CategoryRepository repo;

    @GetMapping("/all")
    public ResponseEntity<?> getAll(){
        List<CategoryEntity> lst = repo.findAll();
        CategoryDto dto = CategoryDto.builder().id(lst.get(0).getId()).name(lst.get(0).getName()).build();
        return ResponseEntity.ok(repo.findAll());
    }

    @GetMapping("/getby{id}")
    public ResponseEntity<?> getAll(@RequestParam("id")int id){
        System.out.println("called");
        return ResponseEntity.ok(repo.findById(id).get());
    }

}
