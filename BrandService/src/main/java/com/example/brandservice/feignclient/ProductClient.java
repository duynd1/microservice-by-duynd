package com.example.brandservice.feignclient;

import com.example.brandservice.dto.ProductDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "product-service")
@Service
public interface ProductClient {
    @SuppressWarnings("unchecked")
    @GetMapping("/getbybrand{brandId}")
    List<ProductDto> getByBrandId(@RequestParam("brandId") int brandId);
}
