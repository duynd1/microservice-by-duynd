package com.example.brandservice.dto;


import lombok.*;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder(toBuilder = true)
@Component
@ToString
public class ProductDto {
    private int id;
    private String name;
    private double price;
    private int quantity;
    private int categoryId;
    private int brandId;
    private CategoryDto category;
    private BrandDto brand;
}
