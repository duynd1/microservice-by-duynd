package com.example.brandservice.dto;

import lombok.*;
import org.springframework.stereotype.Component;

@Component
@Data
@Builder(toBuilder = true)
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class BrandDto {
    private int id;
    private String name;
}
