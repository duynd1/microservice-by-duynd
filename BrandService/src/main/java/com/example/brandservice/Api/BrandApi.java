package com.example.brandservice.Api;

import com.example.brandservice.dto.ProductDto;
import com.example.brandservice.feignclient.ProductClient;
import com.example.brandservice.service.BrandService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/")
public class BrandApi {
    @Autowired
    BrandService brandService;
    @Autowired
    ProductClient productClient;
    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/all")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(brandService.getAll());
    }

    @GetMapping("/getby{id}")
    public ResponseEntity<?> getById(@RequestParam Integer id){
        return ResponseEntity.ok(brandService.getById(id));
    }
    @HystrixCommand(fallbackMethod = "fallback")
    @GetMapping("/getprouductby{brandId}")
    public ResponseEntity<?> getByBrandId(@RequestParam Integer brandId){
        System.out.println("call");
        return ResponseEntity.ok(productClient.getByBrandId(brandId));
    }


    public ResponseEntity<?> fallback(Integer brandId, Throwable hystrixCommand) {
        System.out.println("Product-service die");
        ResponseEntity responseEntity = restTemplate.getForEntity("/getbybrand?brandId="+brandId,Object.class);
        List<ProductDto> listProductByBrand = (List<ProductDto>) responseEntity.getBody();
        return ResponseEntity.ok(listProductByBrand);
    }

}
