package com.example.brandservice.service;

import com.example.brandservice.dto.BrandDto;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface BrandService {
    boolean create(BrandDto brandDto);
    boolean update(BrandDto brandDto);
    boolean delete(Integer id);
    BrandDto getById(Integer id);
    List<BrandDto> getAll();
}
