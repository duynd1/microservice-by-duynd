package com.example.brandservice.service.Ipm;

import com.example.brandservice.dto.BrandDto;
import com.example.brandservice.entity.BrandEntity;
import com.example.brandservice.repository.BrandRepository;
import com.example.brandservice.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
@Service
public class BrandServiceIpm implements BrandService {
    @Autowired
    BrandRepository repo;

    @Override
    public boolean create(BrandDto brandDto) {
        try {
            repo.save(castToEntity(brandDto));
            return true;
        }catch (Exception x){
            x.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean update(BrandDto brandDto) {
        try {
            repo.save(castToEntity(brandDto));
            return true;
        }catch (Exception x){
            x.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(Integer id) {
        try {
            repo.deleteById(id);
            return true;
        }catch (Exception x){
            x.printStackTrace();
            return false;
        }
    }

    @Override
    public BrandDto getById(Integer id) {
        return castToDto(repo.findById(id).get());
    }

    @Override
    public List<BrandDto> getAll() {
        return repo.findAll().stream().map(b->castToDto(b)).collect(Collectors.toList());
    }

    private BrandEntity castToEntity(BrandDto dto){
        return BrandEntity.builder().id(dto.getId()).name(dto.getName()).build();
    }

    private BrandDto castToDto(BrandEntity entity){
        return BrandDto.builder().id(entity.getId()).name(entity.getName()).build();
    }
}
