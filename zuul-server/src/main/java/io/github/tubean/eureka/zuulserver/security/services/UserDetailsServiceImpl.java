package io.github.tubean.eureka.zuulserver.security.services;

import io.github.tubean.eureka.zuulserver.feignclient.UserClient;
import io.github.tubean.eureka.zuulserver.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

  @Autowired
  PasswordEncoder encoder;
  @Autowired
  UserClient client;
  @Override
  @Transactional
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = client.findByUsername(username);
    System.out.println(user.getPassword());
    user.setRole("admin");
    return UserDetailsImpl.build(user);
  }

}
