package io.github.tubean.eureka.zuulserver.model;

import lombok.Data;

@Data
public class JwtResponse {
    private String username;
    private String jwt;
}
