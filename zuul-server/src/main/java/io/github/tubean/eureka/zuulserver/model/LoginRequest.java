package io.github.tubean.eureka.zuulserver.model;


import lombok.Data;

@Data
public class LoginRequest {
    private String username;
    private String password;
}
