package io.github.tubean.eureka.zuulserver.feignclient;

import io.github.tubean.eureka.zuulserver.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Service
@FeignClient(name = "user-service")
public interface UserClient {
    @GetMapping(value = "/getby{username}")
    User findByUsername(@RequestParam("username")String username);
}
